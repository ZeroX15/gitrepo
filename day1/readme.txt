# Dia 1 da Sprint 1

## O que ocorreu?

*Primeiramente, esse deveria ser o dia 2 da sprint, mas fomos orientados a não começar até termos uma reunião com nosso Scrum Master (SM), mas no dia seguinte fomos autorizados a iniciar.*

Nesse primeiro dia, iniciei assistindo a algumas aulas pela trilha da Udemy, sobre alguns fundamentos do Git, o básico do básico:
- Como criar um repositório;
- Como adicionar arquivos ao repositório;
- Como remover arquivos do repositório;
- Quais as diferenças entre o versionamento distribuído e o centralizado;
- O ciclo de vida ou os quatro estágios de um arquivo em um repositório; e
- Como salvar/commitar arquivos no repositório.

Em seguida, o SM iniciou uma reunião com a equipe para poder explicar sobre as Sprints, a challenge atual, tirar algumas dúvidas, entre outros assuntos.

## Como o dia finalizou?

Comecei criando uma pasta chamada **gitrepo** (porque decidi usar o mesmo nome como estava nas videoaulas), nisso, criei uma outra pasta chamada **day1**, dessa forma colocarei cada resumo do dia em readmes que estarão em pastas relacionadas a cada dia.

Então eu vou criar uma issue no GitLab para "formalizar", ao invés de só salvar localmente.

